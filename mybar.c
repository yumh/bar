#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <X11/Xft/Xft.h>
#include <X11/Xlib.h>

#include "modules.h"

#define CHARGE_FULL "/sys/class/power_supply/BAT0/charge_full"
#define CHARGE_NOW "/sys/class/power_supply/BAT0/charge_now"

int
batterymodule (){
  FILE *full, *now;
  int n, m;
  long hfull, know;
  char *sfull, *snow;

  full = fopen(CHARGE_FULL, "r");
  if (full == NULL){
    printf("%s\n","il file full non si apre ");
    return 1;
  }

  now = fopen (CHARGE_NOW, "r");
  if (now == NULL){
    printf ("il file now non si apre ");
    fclose (full);
    return 1;
  }

  fseek(full, 0, SEEK_END);
  fseek(now, 0, SEEK_END);

  n = ftell(full);
  m = ftell(now);

  printf("%d %d\n", n, m);

  sfull = malloc(n+1);
  snow = malloc(m+1);

  rewind (full); //non so cosa sto facendo
  rewind (now); //non so cosa sto facendo
  fread(sfull, 1, n, full);
  fread (snow, 1, m, now);

  hfull = strtol(sfull, NULL, 10);
  know = strtol(snow, NULL, 10);

  printf ("%ld, %ld\n", hfull, know);

  int p = (know*100/ hfull);
  printf ("%d\n", p);

  fclose(full);
  fclose(now);

  return p;
}

/* Get the width and height of the window `w' */
void
get_wh(Display *d, Window *w, int *width, int *height)
{
  XWindowAttributes win_attr;
  
  XGetWindowAttributes(d, *w, &win_attr);
  if (height != NULL)
    *height = win_attr.height;

  if (width != NULL)
    *width = win_attr.width;
}

int
text_extents(unsigned char *str, int len, Display *d, XftFont *font, int *ret_width, int *ret_height)
{
	int	height;
	int	width;
	XGlyphInfo	gi;
	XftTextExtentsUtf8(d, font, str, len, &gi);
	height = gi.y - gi.height ;//- gi.y;
	width = gi.width - gi.x;
	if (ret_width != NULL)  *ret_width = width;
	if (ret_height != NULL) *ret_height = height;
	return width;
}

void
draw (Display *d, Window w, GC bg, XftDraw *xd, XftColor *fg, XftFont *font, int width, int height)
{
  char batteria [10];
  int percentuale = batterymodule();
  snprintf (batteria, 10, "BAT %d%%", percentuale);

  unsigned char msg[256] = {0};
  time_t  t = time(NULL);
  struct tm *now = localtime(&t);
  strftime((char*)msg, sizeof(msg), "%d/%m/%Y %H:%M", now);

  printf("rendering %d+%d:%dx%d -- (%ld)\n", 0, 0, width, height, sizeof(msg));

  XFillRectangle(d, w, bg, 0, 0, width, height);
  int a, l;
  text_extents(msg, strlen((char*)msg), d, font, &l, &a);

  XftDrawStringUtf8(xd, fg, font, 10, 15, (unsigned char*)batteria,strlen((char*)batteria));
  XftDrawStringUtf8(xd, fg, font, width-l-10, 15, msg, strlen((char*)msg));
  XFlush(d);
}

/* Set some WM stuff */
void
set_win_atoms(Display *d, Window w, int width, int height){

  Atom atom;
  atom = XInternAtom(d, "_NET_WM_WINDOW_TYPE_DOCK", 0);

  XChangeProperty(d,
		  w,
		  XInternAtom(d, "_NET_WM_WINDOW_TYPE", 0),
		  XInternAtom(d, "ATOM", 0),
		  32,
		  PropModeReplace,
		  (unsigned char*)&atom, 1);

  atom = XInternAtom(d, "_NET_WM_STATE", 0);
  XChangeProperty(d,
		  w,
		  XInternAtom(d, "_NET_WM_STATE_STICKY", 0),
		  XInternAtom(d, "ATOM", 0),
		  32,
		  PropModeReplace,
		  (unsigned char*)&atom,
		  1);
}

int
main (void)
{
  Display              *d;
  Window               root, bar;
  XSetWindowAttributes attr;
  int                  width, height, child_pid;
  XVisualInfo          vinfo;
  Colormap             cmap;
  XColor               f, v;
  GC                   bg;
  XftDraw              *xd;
  XftColor             fg;
  XftFont              *font;
  XEvent               e;

  /* Ignore the child status */
  signal(SIGCHLD, SIG_IGN);

  if ((d = XOpenDisplay(NULL)) == NULL)
    return 1;

  root = DefaultRootWindow(d);
  get_wh(d, &root, &width, NULL);
  height = 20;

  XMatchVisualInfo(d, DefaultScreen(d), 32, TrueColor, &vinfo);

  cmap = XCreateColormap(d, root, vinfo.visual, AllocNone);

  attr.border_pixel = 0;
  attr.background_pixel = 0x90909090;
  attr.colormap = cmap;
  attr.event_mask = StructureNotifyMask | ExposureMask | VisibilityChangeMask;
  bar = XCreateWindow(d, root, 0, 0, width, height, 0, vinfo.depth, InputOutput, vinfo.visual, CWBorderPixel | CWBackPixel | CWColormap | CWEventMask, &attr);

  set_win_atoms(d, bar, width, height);

  XMapWindow (d, bar);

  bg = XCreateGC (d, bar, 0, NULL);
  XAllocNamedColor(d, cmap, "#1754b1", &f, &v);
  XSetForeground(d, bg, f.pixel);
  XSetBackground(d, bg, v.pixel);

  xd = XftDrawCreate(d, bar, vinfo.visual, cmap);

  {
    XRenderColor xr;
    xr.alpha = 0xffff;
    xr.red = 0xffff;
    xr.green = 0xffff;
    xr.blue = 0xffff;

    XftColorAllocValue(d, vinfo.visual, cmap, &xr, &fg);
  }
  
  font = XftFontOpenName(d, DefaultScreen(d), "DejaVu Mono:style=Book:size=10");

  switch ((child_pid = fork())) {
  case -1:
    fprintf(stderr, "Francesco colpa tua! Anche oggi si forka domani.\n");
    break;

  case 0:
    // figlio
    child_main(bar);
    return 0;

  default:
    break;
  }

  while(1){
    XNextEvent(d, &e);

    switch (e.type){
    case MapNotify:
    case ConfigureNotify:
    case Expose:
    case ClientMessage:
      get_wh(d, &bar, &width, &height);
      draw(d, bar, bg, xd, &fg, font, width, height);
      break;

    default:
      printf("Evento sconosciuto: %d\n", e.type);
    }
  }

  /* TODO: deallocate xft stuff */

  XDestroyWindow(d, bar);
  XCloseDisplay(d);

  return 0;
}
