#include <stdio.h>
#include <string.h>

#include "modules.h"

int
child_main (Window w)
{
  Display *d;

  if ((d = XOpenDisplay(NULL))==NULL)
    return 1;

  while(1) {
    XClientMessageEvent dummyEvent;
    memset(&dummyEvent, 0, sizeof(XClientMessageEvent));
    dummyEvent.type = ClientMessage;
    dummyEvent.window = w;
    dummyEvent.format = 32;
    XSendEvent(d, w, 0, 0, (XEvent*)&dummyEvent);
    XFlush(d);

    printf("hi! ");
    Status s = XSendEvent(d, w, False, NoEventMask, (XEvent*)&dummyEvent);
    printf("s = %d\n", s);

    sleep(10);
  }

  return 0;
}
