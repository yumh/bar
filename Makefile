CC	?= cc
CFLAGS	 = -g -Wall  `pkg-config --cflags x11 xft`
LIBS	 = `pkg-config --libs x11 xft`

mybar: mybar.c modules.c modules.h
	$(CC) $(CFLAGS) mybar.c modules.c -o mybar $(LIBS)
